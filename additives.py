def validate(label):

    additivesApproved = ('E100', 'E101', 'E103', 'E105', 'E111', 'E121', 'E126', 'E140', 'E161', 'E162', 'E170',
    'E172', 'E174', 'E175', 'E181', 'E182', 'E183', 'E190', 'E200', 'E206', 'E237', 'E262', 'E272', 'E290', 'E300', 'E301',
    'E302', 'E303', 'E304', 'E306', 'E307', 'E308', 'E309', 'E325', 'E326', 'E327', 'E331', 'E332', 'E333', 'E334', 'E335',
    'E336', 'E337', 'E400', 'E401', 'E402', 'E403', 'E404', 'E405', 'E406', 'E408', 'E410', 'E411', 'E420', 'E422', 'E440',
    'E470', 'E471', 'E472', 'E473', 'E474', 'E475')

    additivesToxic = ('E102', 'E104', 'E107', 'E110', 'E120', 'E122', 'E123', 'E124', 'E125', 'E127', 'E128', 'E129',
    'E131', 'E132', 'E133', 'E141', 'E142', 'E143', 'E150', 'E151', 'E153', 'E154', 'E155', 'E160', 'E171', 'E173', 'E180',
    'E201', 'E202', 'E203', 'E210', 'E211', 'E212', 'E213', 'E214', 'E215', 'E216', 'E217', 'E220', 'E221', 'E222', 'E223',
    'E224', 'E225', 'E226', 'E227', 'E228', 'E230', 'E231', 'E233', 'E236', 'E238', 'E239', 'E240', 'E241', 'E249', 'E250',
    'E251', 'E252', 'E260', 'E280', 'E281', 'E282', 'E283', 'E310', 'E311', 'E312', 'E313', 'E319', 'E320', 'E321', 'E338',
    'E339', 'E341', 'E407', 'E413', 'E414', 'E416', 'E421', 'E430', 'E431', 'E432', 'E433', 'E434', 'E435', 'E436', 'E441',
    'E450', 'E461', 'E463', 'E465', 'E466', 'E467', 'E480', 'E518', 'E536', 'E553', 'EG20', 'E621', 'E622', 'E623', 'E624',
    'E625', 'E627', 'E631', 'E635', 'E903', 'E905', 'E924', 'E925', 'E926', 'E928', 'E950', 'E951', 'E952', 'E954', 'E1202',
    'E1403')

    seriesToxic = []
    seriesApproved = []
    seriesUnclassified = []

    for additive in label:
        if additive in additivesToxic:
            seriesToxic.append(additive)
        elif additive in additivesApproved:
            seriesApproved.append(additive)
        elif additive not in  additivesToxic and additive not in additivesApproved:
            seriesUnclassified.append(additive)

    print("TOXIC ADDITIVES"  + "\n")
    for additiveToxic in seriesToxic:
        print("• " + additiveToxic + "\n")

    print("APPROVED ADDITIVES"  + "\n")
    for additiveApproved in seriesApproved:
        print("• " + additiveApproved + "\n")

    print("UNCLASSIFIED ADDITIVES"  + "\n")
    for additiveUnclassified in seriesUnclassified:
        print("• " + additiveUnclassified + "\n")

validate(('E330', 'E400', 'E401', 'E402', 'E100', 'E210', 'E211', 'E212', 'E407', 'E1417', 'E1422'))
