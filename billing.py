def calculateBill(onTime, unitsPrimaryMachine, unitsSecondaryMachine, invoice, tenants, splittings):

    # SUPPLIER RATES
    rateDiscounted = 0.201738066095
    rateStandard = 0.309965429604

    # TENANT DISCOUNTS
    consumptionDiscounts = {tenants[0]: False, tenants[1]: False, tenants[2]: True}
    
    # HOUSEHOLD INORMATION DEPENDING ON TENANTS SUPPLIED
    tenantsHousehold = len(tenants)
    tenantsList = list(tenants)

    # SEE HOW MANY TENANTS ARE ELIGIBLE FOR DEDUCTION
    tenantsReduced = list(consumptionDiscounts.values()).count(True)
    
    tenantDeduction = 0
    tenantsList.append('Machines')

    # SEE WHETHER SUPPLIER DISCOUNT IS APPLICABLE OR NOT
    if(onTime):
        consumptionMachines = (unitsPrimaryMachine + unitsSecondaryMachine) * rateDiscounted        
    else:
        consumptionMachines = (unitsPrimaryMachine + unitsSecondaryMachine) * rateStandard    

    # DEDUCT THE MACHINES' CONSUMPTION FROM HOUSEHOLD
    consumptionHousehold = invoice - consumptionMachines

    # DISTRIBUTE HOUSEHOLD CONSUMPTION BASED ON SPLITTINGS
    consumptionShared = consumptionHousehold / tenantsHousehold
    consumptionAllocated = {tenantsList[0]: splittings[0] * consumptionShared, tenantsList[1]: splittings[1] * consumptionShared, tenantsList[2]: splittings[2] * consumptionShared, tenantsList[3]: consumptionMachines}

    # SEE WHETHER TENANT IS ELIGIBLE FOR DEDUCTION OR NOT  
    for tenantDiscounted in consumptionDiscounts.items():
        if tenantDiscounted[1]:        
            tenantDeduction += consumptionAllocated[tenantDiscounted[0]]

    # CALCULATE THE CONSUMPTION SHARED BETWEEN TENANTS WITHOUTH DEDUCTIONS
    consumptionShared = (consumptionHousehold - tenantDeduction) / (tenantsHousehold - tenantsReduced)

    # SEE THAT OTHER TENANTS ARE CORRECTLY BILLED
    for tenantDiscounted in consumptionDiscounts.items():

        # WHEN TENANT IS 0 MACHINE CONSUMPTION IS ADDED
        if not tenantDiscounted[1] and tenantDiscounted[0] is not tenantsList[0]:        
            consumptionAllocated[tenantDiscounted[0]] = consumptionShared
        elif tenantDiscounted[0] is tenantsList[0]:
            consumptionAllocated[tenantsList[0]] = consumptionShared + consumptionMachines

    return consumptionAllocated

def plotInformation(bill):

    # GETS BILL INFORMATION
    allocations = bill.items()

    # ITERATES OVER BILL INFORMATION AND OUTPUTS TO CONSOLE
    for tenantAllocation in allocations:
        print(str(tenantAllocation[0]).ljust(40, ".") + ": $" + str(round(tenantAllocation[1], 2)).zfill(6) + "\n")

# SPECIFYING WHETHER PAYMENT IS ON TIME OR NOT
prompt = input("ON TIME? ")

if "N" in prompt or "n" in prompt:
    onTime = False
elif "Y" in prompt or "y" in prompt:
    onTime = True
else:
    onTime = False

# SUBMITTING TENANT NAMES
tenants = ['', '', '']
tenants[0] = input("TENANT 1".ljust(40, ".") + ": ").title()
tenants[1] = input("TENANT 2".ljust(40, ".") + ": ").title()
tenants[2] = input("TENANT 3".ljust(40, ".") + ": ").title()

# SUBMITTING BILL SPLITTINGS
splittings = [0, 0, 0]
splittings[0] = int(input("SPLITTING 1".ljust(40, ".") + ": "))
splittings[1] = int(input("SPLITTING 2".ljust(40, ".") + ": "))
splittings[2] = int(input("SPLITTING 3".ljust(40, ".") + ": "))

# SUBMITTING BILL DETAILS
details = [0.0, 0.0, 0.0]
details[0] = float(input("MACHINE I UNITS".ljust(40, ".") + ": "))
details[1] = float(input("MACHINE II UNITS".ljust(40, ".") + ": "))
details[2] = float(input("INVOICE AMOUNT".ljust(40, ".") + ": "))

# INVOKING THE BILLING FUNCTION
monthlyBilling = calculateBill(onTime, details[0], details[1], details[2], (tenants[0], tenants[1], tenants[2]), (splittings[0], splittings[1], splittings[2]))

# INVOKING THE PLOTTING FUNCTION
plotInformation(monthlyBilling)
